// Usage: deno bbSshPipelines.ts --allow-run --allow-env --allow-net --repo "repo-name"
// Ensure BBUSER and BBPASS are set as env vars for the user to add the SSH key too
// Ensure BBADMINUSER and BBADMINPASS are set as env vars for a user which is an admin of the repo in question

import { parse } from 'https://deno.land/std/flags/mod.ts';
import * as base64 from 'https://denopkg.com/chiefbiiko/base64/mod.ts';

const { args, env } = Deno;

const createAuthB64 = (username: string, password: string) =>
    base64.fromUint8Array(new TextEncoder().encode(`${username}:${password}`));

const createAuthorizedJsonHeaders = (bbUser: string, bbPass: string) => {
    const headers = new Headers();
    const auth = createAuthB64(bbUser, bbPass);
    headers.set('Authorization', `Basic ${auth}`);
    headers.set('Content-Type', 'application/json');
    return headers;
};

const generateSshKeysOnPipeline = async (repoName: string, bbUser: string, bbPass: string) => {
    const headers = createAuthorizedJsonHeaders(bbUser, bbPass);
    const bbUrl = `https://api.bitbucket.org/internal/repositories/palletech/${repoName}/pipelines_config/ssh/key_pair/generate`;
    const req = new Request(bbUrl, { method: 'POST', body: JSON.stringify({}), headers });
    return fetch(req);
};

const addSshKeyToUser = async (repoName: string, publicKey: string, bbUser: string, bbPass: string) => {
    const headers = createAuthorizedJsonHeaders(bbUser, bbPass);
    const bbUrl = `https://api.bitbucket.org/2.0/users/${bbUser}/ssh-keys`;
    const req = new Request(bbUrl, {
        method: 'POST',
        body: JSON.stringify({
            key: publicKey,
            label: `${repoName}-${new Date().toISOString()}`,
        }),
        headers,
    });
    return fetch(req);
};

const main = async () => {
    const { BBUSER, BBPASS, BBADMINUSER, BBADMINPASS } = env();
    if (!BBUSER || !BBPASS || !BBADMINUSER || !BBADMINPASS)
        throw new Error(`BBUSER, BBPASS, BBADMINUSER and BBADMINPASS must be set as env vars`);

    const { repo: repoName } = parse(args);
    if (!repoName) throw new Error(`Repo name (--repo) MUST be specified`);

    const { error: sshKeyGenErr, public_key: publicKey } = await generateSshKeysOnPipeline(
        repoName,
        BBADMINUSER,
        BBADMINPASS,
    ).then(res => res.json());
    if (sshKeyGenErr) throw new Error(`${sshKeyGenErr.message}: ${sshKeyGenErr.detail}`);
    console.log(`Generated public key for repo ${repoName}: ${publicKey.substr(0, 30)}...`);
    console.log(`Adding public key to user ${BBUSER}`);
    const sshKeyResp = await addSshKeyToUser(repoName, publicKey, BBUSER, BBPASS).then(res => res.json());
    console.log(JSON.stringify(sshKeyResp));
    return null;
};

main().catch(err => {
    console.error(err);
});
