// Usage: deno bbPr.ts --allow-run --allow-env --allow-net -c "Commitlint-compliant commit title"
// Ensure BBUSER and BBPASS are set as env vars

import { parse } from 'https://deno.land/std/flags/mod.ts';
import * as base64 from 'https://denopkg.com/chiefbiiko/base64/mod.ts';

const { args, env, run } = Deno;

const REVIEWERS_LIST = ['anwain', 'joshpt'];

const createAuthB64 = (username: string, password: string) =>
    base64.fromUint8Array(new TextEncoder().encode(`${username}:${password}`));

const getSafeSubprocessOutput = async (args: string[]) => {
    const p = run({
        args,
        stdout: 'piped',
        stderr: 'piped',
    });
    const { code } = await p.status();
    if (code === 0) {
        const buf = await p.output();
        return new TextDecoder().decode(buf).trim();
    } else {
        throw new Error(`Deno got error code ${code} whilst executing ${args.join(' ')}`);
    }
};

const extractMainRemoteBranchFromRemoteShowOrigin = remoteShowOriginStdOut => {
    const lines: string[] = remoteShowOriginStdOut.split('\n');
    const [remoteBranchLine] = lines.filter(str => str.includes('HEAD branch'));
    const partsOfRemoteBranchLine = remoteBranchLine.split(' ');
    return partsOfRemoteBranchLine[partsOfRemoteBranchLine.length - 1];
};

const removeGitHashFromStartOfCommitMsg = (logMsg: string) => {
    const [, ...actualMsgWithoutHash] = logMsg.split(' ');
    return actualMsgWithoutHash.join(' ');
};

const prependAsterisk = (str: string) => `* ${str}`;

const extractRepoOwnerAndRepoNameFromGitRemoteOrigin = (remoteOrigin: string) => {
    const [, nameAndRepo] = remoteOrigin.split(':');
    const [repoOwner, repoNameWithGitExtension] = nameAndRepo.split('/');
    const [repoNameOnly] = repoNameWithGitExtension.split('.');
    return [repoOwner, repoNameOnly];
};

const main = async () => {
    const bbuser = env().BBUSER;
    const bbpassword = env().BBPASS;
    if (!bbuser || !bbpassword) throw new Error(`BBUSER and BBPASS must be set as env vars`);

    const {
        d: cliDestinationBranch,
        s: cliSourceBranch,
        t: cliPrTitle,
        f: cliShouldForceBranchPush,
        c: cliCommitTitle,
    } = parse(args);
    if (!cliCommitTitle) throw new Error(`Commit title (-c) MUST be specified`);

    const gitRemoteOrigin = await getSafeSubprocessOutput(['git', 'config', '--get', 'remote.origin.url']);
    const [repoOwner, repoNameOnly] = extractRepoOwnerAndRepoNameFromGitRemoteOrigin(gitRemoteOrigin);
    const currentGitUserName = await getSafeSubprocessOutput(['git', 'config', '--get', 'user.name']);
    const sourceBranch =
        cliSourceBranch || (await getSafeSubprocessOutput(['git', 'rev-parse', '--abbrev-ref', 'HEAD']));
    const destinationBranch =
        cliDestinationBranch ||
        extractMainRemoteBranchFromRemoteShowOrigin(await getSafeSubprocessOutput(['git', 'remote', 'show', 'origin']));
    const prTitle = cliPrTitle || sourceBranch;
    // Make sure destination branch is up to date
    await getSafeSubprocessOutput(['git', 'fetch', 'origin', `${destinationBranch}:${destinationBranch}`]);
    const commitMsgsSinceDestinationBranch = (
        await getSafeSubprocessOutput(['git', 'log', `${destinationBranch}..${sourceBranch}`, '--oneline'])
    )
        .split('\n')
        .filter(commitMsg => commitMsg !== '')
        .map(removeGitHashFromStartOfCommitMsg)
        .map(prependAsterisk);
    const fullCommitMsg = `${cliCommitTitle}\n\n${commitMsgsSinceDestinationBranch.join('\n')}`;

    const reviewersObjList = REVIEWERS_LIST.filter(reviewer => reviewer !== currentGitUserName).map(reviewer => ({
        username: reviewer,
    }));

    await getSafeSubprocessOutput(['git', 'reset', '--soft', `HEAD~${commitMsgsSinceDestinationBranch.length}`]);
    await getSafeSubprocessOutput(['git', 'commit', '-m', fullCommitMsg, '--no-verify']);

    const gitPushArgs = ['git', 'push', 'origin', sourceBranch];
    if (cliShouldForceBranchPush) gitPushArgs.push('--force');
    await getSafeSubprocessOutput(gitPushArgs);

    const headers = new Headers();
    const auth = createAuthB64(bbuser, bbpassword);
    headers.set('Authorization', `Basic ${auth}`);
    headers.set('Content-Type', 'application/json');
    const bbUrl = `https://api.bitbucket.org/2.0/repositories/${repoOwner}/${repoNameOnly}/pullrequests`;
    const body = {
        title: prTitle,
        summary: { raw: fullCommitMsg },
        source: { branch: { name: sourceBranch } },
        destination: { branch: { name: destinationBranch } },
        reviewers: reviewersObjList,
        close_source_branch: true,
    };

    const req = new Request(bbUrl, { method: 'POST', body: JSON.stringify(body), headers });
    return fetch(req);
};

main().catch(err => {
    console.error(err);
});
