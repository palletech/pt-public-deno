# [0.3.0](https://bitbucket.org/palletech/pt-public-deno/compare/0.3.0%0D0.2.1) (2019-12-10)


### Features

* **bbSsh:** add script to create ssh key on repo pipeline and attach to user ([c72fae6](https://bitbucket.org/palletech/pt-public-deno/commits/c72fae6270b18d58bf04a879ffe54c0f5c0d0d8e))



## [0.2.1](https://bitbucket.org/palletech/pt-public-deno/compare/0.2.1%0D0.2.0) (2019-11-27)


### Bug Fixes

* **fixOnEmptyCommit:** fix empty asterisk ([1331bd2](https://bitbucket.org/palletech/pt-public-deno/commits/1331bd2c2427f578ac02437799a778100c466dd2))



# [0.2.0](https://bitbucket.org/palletech/pt-public-deno/compare/0.2.0%0D0.1.1) (2019-11-26)


### Features

* **ptbb:** add ptbb logic to Deno script ([ed52d55](https://bitbucket.org/palletech/pt-public-deno/commits/ed52d55d90386f8273ba9fb43020f5f008dd796e))



## 0.1.1 (2019-11-26)


### Bug Fixes

* **bumpver:** remove serverless listing from bumpversion config ([8b26206](https://bitbucket.org/palletech/pt-public-deno/commits/8b26206f7d13478f655ed92e5625fff2c6a1a15f))


### Features

* **config:** add config files like linting, makefile and pipelines ([1795972](https://bitbucket.org/palletech/pt-public-deno/commits/17959728f04ab1994492f774fb867acd903b996b))
* **initial:** add gitignore ([095c6a0](https://bitbucket.org/palletech/pt-public-deno/commits/095c6a0fcf252e6e8affc1d4381d78ef9e95098c))



