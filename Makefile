
##
## PT Node Makefile
##  ____   ___    _   _  ___ _____   _____ ____ ___ _____
## |  _ \ / _ \  | \ | |/ _ \_   _| | ____|  _ \_ _|_   _|
## | | | | | | | |  \| | | | || |   |  _| | | | | |  | |
## | |_| | |_| | | |\  | |_| || |   | |___| |_| | |  | |
## |____/ \___/  |_| \_|\___/ |_|   |_____|____/___| |_|
##
##
##  DEPENDENCIES
##================
## * UNIX operating system
## * GNU Make >= 3.82
## * node == 10.17
## * yarn >= 1.15
## * aws-cli >= 1.16

SHELL := /bin/bash
CWD := $(shell pwd)
CARGO_HOME = ~/.cargo/bin
DIRNAME := $(notdir $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST))))))
GIT_BRANCH_TYPE := $(shell git rev-parse --abbrev-ref HEAD | $(CARGO_HOME)/sd '(.*)(/.*)' '$$1')
STAGE ?= dev
COVFILENAME = COVERAGE
COVREPORTDIR = coverage
DOCDIR = out
VERSION := $(shell node -e "console.log(require('./package.json').version)")
EXTERNAL_DEPS_FILE = extra.mk
S3_COV_BUCKET = pt-project-coverage
S3_DOCS_BUCKET = pt-project-docs

# Cargo constants
# TODO: Move these to a package.json like file

CARGO_PARALLEL_VER = 0.11.3
CARGO_RG_VER = 11.0.1
CARGO_SD_VER = 0.6.5
CARGO_FD_VER = 7.4.0

-include $(EXTERNAL_DEPS_FILE)

# Toolchain commands

checkdep = command -v $(1) >/dev/null 2>&1 || { echo >&2 "I require $(1). Check your $(1) installation"; exit 1; }
checkver = $(1) --version | awk '/$(2)/ {rc = 1;}; END { exit !rc }' && exit 0 || \
		   { echo "I require $(1) version $(2). \
		   Check your $(1) installation"; exit 1; }
cargo_install = cargo install $(1) --vers $(2) --force

all: check lint test

## -- Toolchain Targets --

install_rust:
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
	source $$HOME/.cargo/env
	echo "Please add source $HOME/.cargo/env to your PATH"

install_rust_deps:
	@$(call cargo_install,parallel,$(CARGO_PARALLEL_VER))
	@$(call cargo_install,ripgrep,$(CARGO_RG_VER))
	@$(call cargo_install,sd,$(CARGO_SD_VER))
	@$(call cargo_install,fd-find,$(CARGO_FD_VER))

## -- QA Targets --

## Test project
test:
	yarn test

## Lint project
lint:
	yarn lint

## -- Documentation Targets --

## Run a coverage report
coverage_report:
	mkdir -p $(COVREPORTDIR)
	yarn coverage

## Run a coverage report and upload the static files to S3
coverage_up:
	yarn coverageReport
	aws s3 sync $(COVREPORTDIR) s3://$(S3_COV_BUCKET)/$(DIRNAME)
	aws s3 cp $(COVREPORTDIR)/$(COVFILENAME) s3://$(S3_COV_BUCKET)/$(DIRNAME)/$(COVFILENAME) --acl public-read

## Create docs
doc:
	mkdir -p $(DOCDIR)
	yarn docs || exit 0

## Create docs and upload them to S3
doc_up: doc
	aws s3 sync ${DOCDIR} s3://$(S3_DOCS_BUCKET)/$(DIRNAME)

## -- Installation Targets --

## Check installation of core dependencies
check:
	@$(call checkdep,aws)
	@$(call checkdep,node)
	@$(call checkdep,yarn)
ifeq ($(CI),true)
	@$(call checkdep,bumpversion)
	@$(call checkdep,conventional-changelog)
endif

## Compile project
compile:
	@if grep -q \"build\"\: package.json; then yarn build; else echo "Skipping yarn build"; fi

## Install project with dev dependencies
install_dev:
ifeq ($(CI),true)
	@yarn install --mutex network --frozen-lockfile
else
	@yarn install --mutex network
endif

## Update project
update:
	git pull --rebase
	yarn

## Clean project node dependencies
clean:
	rm -rf node_modules

## -- Packaging & Deployment Targets --

## Deploys to stage. Make sure to add the variable STAGE=foo
deploy:
ifeq ($(CI),true)
	yarn deploy ${STAGE}
else
	@echo "CI not set to true, not deploying"
endif

## -- Misc Targets --

## Bumpversion and changelog generation
bump_cl:
ifeq ($(CI),true)
	@{ \
	GIT_LAST_MERGED_BRANCH_TYPE=$$(git show :/^Merged | $(CARGO_HOME)/rg 'Merged in' | $(CARGO_HOME)/sd '(.*Merged in )(.*)(/.*)' '$$2') ;\
	GIT_PATCH_BRANCHES="patch build ci docs fix refactor test" ;\
	if [[ $$GIT_PATCH_BRANCHES =~ $$GIT_LAST_MERGED_BRANCH_TYPE ]]; then BUMPVERSION_TYPE=patch; \
	elif [[ $$GIT_LAST_MERGED_BRANCH_TYPE == feat ]]; then BUMPVERSION_TYPE=minor; \
	elif [[ $$GIT_LAST_MERGED_BRANCH_TYPE == major ]]; then BUMPVERSION_TYPE=major; \
	else echo "No bumping on $$GIT_LAST_MERGED_BRANCH_TYPE" && exit 0; \
	fi;\
	OLD_VERSION=$$(node -e "console.log(require('./package.json').version)") && \
	bumpversion $$BUMPVERSION_TYPE && \
	NEW_VERSION=$$(node -e "console.log(require('./package.json').version)") && \
	git add --all && \
	git commit -m "[BUMPVERSION]: $$NEW_VERSION [skip ci]" && \
	conventional-changelog -p angular -i CHANGELOG.md -s && \
	$(CARGO_HOME)/sd "($$OLD_VERSION)(...)($$NEW_VERSION)" '$$3%0D$$1' CHANGELOG.md ; \
	git add CHANGELOG.md && \
	git commit -m "ci(bump): bumpversion to $${NEW_VERSION}, update changelog [skip ci]" && \
	git push -u origin $$BITBUCKET_BRANCH ;\
	}
else
	@echo "CI not set to true, not bumpversioning"
endif

## Add git tag
tag:
	git tag $(VERSION) || echo "Tag already exists"
	git push origin refs/tags/$(VERSION) --no-verify

## Run docker locally from CI/CD image (check bitbucket-pipelines.yml)
run_docker:
	docker run -v $$(pwd):/app -it $$(head -n 1 bitbucket-pipelines.yml | cut -c8- )

# Credit: https://gist.github.com/prwhite/8168133#gistcomment-2749866
help:
	@printf "Usage\n";

	@awk '{ \
			if ($$0 ~ /^.PHONY: [a-zA-Z\-\_0-9]+$$/) { \
				helpCommand = substr($$0, index($$0, ":") + 2); \
				if (helpMessage) { \
					printf "\033[36m%-20s\033[0m %s\n", \
						helpCommand, helpMessage; \
					helpMessage = ""; \
				} \
			} else if ($$0 ~ /^[a-zA-Z\-\_0-9.]+:/) { \
				helpCommand = substr($$0, 0, index($$0, ":")); \
				if (helpMessage) { \
					printf "\033[36m%-20s\033[0m %s\n", \
						helpCommand, helpMessage; \
					helpMessage = ""; \
				} \
			} else if ($$0 ~ /^##/) { \
				if (helpMessage) { \
					helpMessage = helpMessage"\n                     "substr($$0, 3); \
				} else { \
					helpMessage = substr($$0, 3); \
				} \
			} else { \
				if (helpMessage) { \
					print "\n                     "helpMessage"\n" \
				} \
				helpMessage = ""; \
			} \
		}' \
		$(MAKEFILE_LIST)

.PHONY: test lint coverage_report coverage_up doc doc_up check compile install_dev update clean deploy bump_cl tag run_docker help
